import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ExamModule } from './exam/exam.module';
import { FieldModule } from './field/field.module';
import { LevelModule } from './level/level.module';
import { QuestionModule } from './question/question.module';
import { HistoryModule } from './history/history.module';
import { DetailsModule } from './details/details.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '123456',
      database: 'ttc_project',
      entities: [__dirname + './**/**/*entity{.ts,.js}'],
      autoLoadEntities: true,
      synchronize: true,
    }),
    UserModule,
    ExamModule,
    FieldModule,
    LevelModule,
    QuestionModule,
    HistoryModule,
    DetailsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
