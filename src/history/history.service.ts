/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { History } from './entities/history.entity';

@Injectable()
export class HistoryService {
  constructor(
    @InjectRepository(History)
    private readonly historyRepository: Repository<History>,
  ) {}

  async getAllHistories(): Promise<History[]> {
    return await this.historyRepository.createQueryBuilder('history')
    .leftJoin('history.exam', 'exam')
    .leftJoin('history.user', 'user')
    .select(['history.id as id', 'user.id as user', 'exam.id as exam', 'result', 'date'])
    .getRawMany();
  }

  async getHistoryById(id: number): Promise<History> {
    return await this.historyRepository.createQueryBuilder('history')
      .leftJoinAndSelect('history.exam', 'exam')
      .leftJoinAndSelect('history.user', 'user')
      .where('history.id = :id', {id: id})
      .getOne();
  }

  async getHistoryByUserId(id: number): Promise<History[]> {
    return await this.historyRepository.createQueryBuilder('history')
    .leftJoinAndSelect('history.exam', 'exam')
    .leftJoin('exam.field', 'field')
    .leftJoin('exam.level', 'level')
    .leftJoin('history.user', 'user')
    .addSelect(['user.id', 'field.major', 'level.label'])
    .where('user.id= :id', {id: id})
    .getMany();
  }

  async createHistory(history: History) {
    return await this.historyRepository.save(history);
  }

  async updateHistory(history: History) {
    return await this.historyRepository.save(history);
  }

  async deleteHistory(id: number) {
    return this.historyRepository.delete(id);
  }
}
