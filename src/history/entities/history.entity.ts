/* eslint-disable prettier/prettier */
import { Exam } from './../../exam/entities/exam.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './../../user/entities/user.entity';

@Entity('histories')
export class History {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'date', nullable: true})
  date: Date;
  
  @Column({ type: 'varchar', nullable: true })
  result: string;

  @ManyToOne(
    () => User,
    (user: User) => user.histories,
    {onDelete: 'CASCADE', onUpdate: 'CASCADE'},
  )
  @JoinColumn({name: 'userId'})
  user: User;

  @ManyToOne(
    () => Exam,
    (exam: Exam) => exam.histories,
    {onDelete: 'CASCADE', onUpdate: 'CASCADE'},
  )
  @JoinColumn({name: 'examId'})
  exam: Exam;
}