/* eslint-disable prettier/prettier */
import { History } from './entities/history.entity';
import { HistoryService } from './history.service';
import { Body, Controller, Delete, Get, Param, Post, Put, RequestTimeoutException } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common';
@Controller('history')
export class HistoryController {
  constructor(private readonly historyService: HistoryService) {}

  @Get()
  async getAllHistories(): Promise<History[]> {
    return await this.historyService.getAllHistories();
  }

  @Get(':id')
  async getHistoryById(@Param('id') id: number) {
    const data = await this.historyService.getHistoryById(id);
    if(!data) throw new NotFoundException('History does not exists.');

    return data;
  }

  @Get('user/:id')
  async getExam(@Param('id') id: number) {
    return await this.historyService.getHistoryByUserId(id);
  }

  @Post()
  async createExam(@Body() history: History) {
    const data = await this.historyService.createHistory(history);
    return {
      message: 'History created.',
      data,
    };
  }

  @Put(':id')
  async updateHistory(@Param('id') id: number, @Body('result') result: string) {
    const history = await this.historyService.getHistoryById(id);
    if(!history) throw new NotFoundException('History does not exists.');
    history.result = result;
    await this.historyService.updateHistory(history);
    return "History updated.";
  }

  @Delete(':id')
  async deleteHistory(@Param('id') id: number) {
    const data =await this.historyService.deleteHistory(id);
    if(!data) throw new NotFoundException('History does not exists.');
    return 'History deleted.';
  }
}
