/* eslint-disable prettier/prettier */
import { QuestionService } from './question.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { Question } from './entities/question.entity';
import { Field } from 'src/field/entities/field.entity';
import { Level } from 'src/level/entities/level.entity';
@Controller('question')
export class QuestionController {
  constructor(private readonly questionService: QuestionService) {}

  @Get()
  async getAllQuestions(@Query('field') fieldId: number, @Query('level') levelId: number) {
    if(!fieldId || !levelId) 
      return await this.questionService.getAllQuestions();
    return await this.questionService.getQuestions(fieldId, levelId);
  }

  @Get(':id')
  async getQuestion(@Param('id') id: number) {
    try {
      return await this.questionService.getQuestion(id);
    } catch (err) {
      throw new HttpException('Question does not exists.', 404);
    }
  }

  @Post()
  async createQuestion(
    @Body('content') content: string,
    @Body('solutionA') slnA: string,
    @Body('solutionB') slnB: string,
    @Body('solutionC') slnC: string,
    @Body('solutionD') slnD: string,
    @Body('correct') correct: string,
    @Body('field') field: Field,
    @Body('level') level: Level,
  ) {
    try {
      const question = new Question();
      question.content = content;
      question.solutionA = slnA;
      question.solutionB = slnB;
      question.solutionC = slnC;
      question.solutionD = slnD;
      question.correct = correct;
      question.field = field;
      question.level = level;
      const data = await this.questionService.createQuestion(question);
      return {
        message: 'Question created.',
        data,
      };
    } catch (error) {
      throw new HttpException('fieldId or levelId does not exists.', 404);
    }
  }

  @Post('many')
  async postManyQuestions(@Body() questions: Question[]) {
    const indexErr = [];
    for (let i = 0; i < questions.length; i++) {
      try {
        await this.questionService.createQuestion(questions[i]);
      } catch (err) {
        indexErr.push(i);
      }
    }
    
    return {
      message: (indexErr.length) ? `Something wrong at index ${indexErr.toString()}` : `All questions has been created.`,
    };
  }

  @Put(':id')
  async updateQuestion(
    @Param('id') id: number,
    @Body('content') content: string,
    @Body('solutionA') slnA: string,
    @Body('solutionB') slnB: string,
    @Body('solutionC') slnC: string,
    @Body('solutionD') slnD: string,
    @Body('correct') correct: string,
    @Body('field') field: Field,
    @Body('level') level: Level,
  ) {
    try {
      const question = await this.questionService.getQuestion(id);
      question.content = content;
      question.solutionA = slnA;
      question.solutionB = slnB;
      question.solutionC = slnC;
      question.solutionD = slnD;
      question.correct = correct;
      question.field = field;
      question.level = level;
      const data = await this.questionService.createQuestion(question);
      return {
        message: 'Question created.',
        data,
      };
    } catch (error) {
      throw new HttpException('fieldId or levelId does not exists.', 404);
    }
  }

  @Delete(':id')
  async deleteQuestion(@Param('id') id: number) {
    return await this.questionService.deleteQuestion(id);
  }
}
