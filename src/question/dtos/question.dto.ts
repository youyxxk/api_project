/* eslint-disable prettier/prettier */
export class createQuestionDTO {
  public content: string;
  public solutionA: string;
  public solutionB: string;
  public solutionC: string;
  public solutionD: string;
  public correct: string;
  public answer: string;
  public fieldId: number;
  public levelId: number;
}