/* eslint-disable prettier/prettier */
import { Level } from './../../level/entities/level.entity';
import { Field } from './../../field/entities/field.entity';
import { Details } from 'src/details/entities/details.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('questions')
export class Question {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'varchar', nullable:true})
  content: string;

  @Column({ type: 'varchar', nullable: true })
  solutionA: string;

  @Column({ type: 'varchar', nullable: true })
  solutionB: string;

  @Column({ type: 'varchar', nullable: true })
  solutionC: string;

  @Column({ type: 'varchar', nullable: true })
  solutionD: string;

  @Column({ type: 'varchar', nullable: true })
  correct: string;

  @OneToMany(
    () => Details,
    (details: Details) => details.question,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  details: Details[];

  @ManyToOne(
    () => Field,
    (field: Field) => field.questions,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  @JoinColumn({name: 'fieldId'})
  field: Field;

  @ManyToOne(
    () => Level,
    (level: Level) => level.questions,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  @JoinColumn({name: 'levelId'})
  level: Level;
}
