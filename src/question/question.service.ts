/* eslint-disable prettier/prettier */
import { Question } from './entities/question.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FieldService } from 'src/field/field.service';
@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(Question)
    private readonly questionRepository: Repository<Question>,
  ) {}

  async getAllQuestions(): Promise<Question[]> {
    return await this.questionRepository
      .createQueryBuilder('qs')
      .leftJoinAndSelect('qs.field', 'field')
      .leftJoinAndSelect('qs.level', 'level')
      .getMany();
  }

  async getQuestion(id: number): Promise<Question> {
    return await this.questionRepository
      .createQueryBuilder('qs')
      .leftJoinAndSelect('qs.field', 'field')
      .leftJoinAndSelect('qs.level', 'level')
      .where('qs.id = :id', { id: id })
      .getOne();
  }

  async getQuestions(fieldId: number, levelId: number) {
    return await this.questionRepository
      .createQueryBuilder('qs')
      .leftJoinAndSelect('qs.field', 'field')
      .leftJoinAndSelect('qs.level', 'level')
      .where('qs.field.id = :fieldId', {fieldId: fieldId})
      .andWhere('qs.level.id = :levelId', {levelId: levelId})
      .getMany();
  }

  async createQuestion(question: Question) {
    return this.questionRepository.save(question);
  }

  async updateQuestion(question: Question) {
    return await this.questionRepository.save(question);
  }

  async deleteQuestion(id: number) {
    return await this.questionRepository.delete(id);
  }
}
