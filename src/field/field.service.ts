/* eslint-disable prettier/prettier */
import { Field } from './entities/field.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { NotFoundException } from '@nestjs/common';

@Injectable()
export class FieldService {
  constructor(
    @InjectRepository(Field)
    private readonly fieldRepository: Repository<Field>,
  ) {}

  async getAllFields(): Promise<Field[]> {
    return await this.fieldRepository.find();
  }

  async getField(id: number): Promise<Field> {
    try {
      const field = await this.fieldRepository.findOne(id);
      return field;
    } catch (error) {
      throw new NotFoundException('Field does not exists.');
    }
  }

  async createField(field: Field) {
    return await this.fieldRepository.save(field);
  }

  async updateField(field: Field) {
    return await this.fieldRepository.save(field);
  }

  async deleteField(id: number) {
    return await this.fieldRepository.delete(id);
  }
}
