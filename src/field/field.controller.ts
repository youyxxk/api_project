/* eslint-disable prettier/prettier */
import { Field } from './entities/field.entity';
import { FieldService } from './field.service';
import {
  Body,
  Controller,
  Delete,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Get } from '@nestjs/common';

@Controller('field')
export class FieldController {
  constructor(private readonly fieldService: FieldService) {}

  @Get()
  async getAllFields() {
    return await this.fieldService.getAllFields();
  }

  @Get(':id')
  async getField(@Param('id') id: number) {
    return await this.fieldService.getField(id);
  }

  @Post()
  async createField(@Body() field: Field) {
    // const data = await this.fieldService.createField({ ...dto });
    this.fieldService.createField(field);
    return {
      message: 'Level created.',
      field,
    };
  }

  @Put(':id')
  async updateField(
    @Param('id') id: number,
    @Body()
    { major }: Field,
  ) {
    const field = await this.fieldService.getField(id);
    if (!field) throw new NotFoundException('Field does not exists');
    if (major) {
      field.major = major;
    }

    this.fieldService.updateField(field);
    return { message: 'Field updated.' };
  }

  @Delete(':id')
  deleteField(@Param('id') userId: number) {
    return this.fieldService.deleteField(userId);
  }
}
