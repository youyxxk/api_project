/* eslint-disable prettier/prettier */
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Question } from './../../question/entities/question.entity';
import { Exam } from 'src/exam/entities/exam.entity';

@Entity('fields')
export class Field {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: true })
  major: string;

  @OneToMany(
    () => Question,
    (question: Question) => question.field,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  questions: Question[];

  @OneToMany(
    () => Exam,
    (exam: Exam) => exam.field,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  exams: Exam[];
}
