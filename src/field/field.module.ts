import { Field } from './entities/field.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { FieldController } from './field.controller';
import { FieldService } from './field.service';

@Module({
  controllers: [FieldController],
  providers: [FieldService],
  imports: [TypeOrmModule.forFeature([Field])],
})
export class FieldModule {}
