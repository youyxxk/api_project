/* eslint-disable prettier/prettier */
import { Level } from './../level/entities/level.entity';
import { Field } from './../field/entities/field.entity';
import { ExamService } from './exam.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Exam } from './entities/exam.entity';
@Controller('exam')
export class ExamController {
  constructor(private readonly examService: ExamService) {}

  @Get()
  async getAllExams(): Promise<Exam[]> {
    return await this.examService.getAllExams();
  }

  @Get(':id')
  async getExam(@Param('id') id: number): Promise<Exam> {
    try {
      return await this.examService.getExam(id);
    } catch (error) {
      throw new HttpException('Exam does not exist.', 404);
    }
  }

  @Get('question/:id')
  async getQuestionsByExamId(@Param('id') id: number) {
    return await this.examService.getQuestionsByExamId(id);
  }

  @Post()
  async createExam(
    @Body('questionNumber') questionNumber: number,
    @Body('time') time: number,
    @Body('field') field: Field,
    @Body('level') level: Level,
  ) {
    try {
      const exam = new Exam();
      exam.questionNumber = questionNumber || null;
      exam.time = time || null;
      exam.field = field;
      exam.level = level;
      const data = await this.examService.createExam(exam);
      return {
        message: 'Exam created.',
        data,
      };
    } catch (error) {
      throw new HttpException('fieldId or levelId does not exists.', 404);
    }
  }

  @Put(':id')
  async updateExam(
    @Param('id') id: number,
    @Body('questionNumber') questionNumber: number,
    @Body('time') time: number,
    @Body('field') field: Field,
    @Body('level') level: Level,
  ) {
    try {
      const exam = await this.examService.getExam(id);
      exam.questionNumber = questionNumber || null;
      exam.time = time || null;
      exam.field = field;
      exam.level = level;
      const data = await this.examService.createExam(exam);
      return {
        message: 'Exam Updated.',
        data,
      };
    } catch (error) {
      throw new HttpException('fieldId or levelId does not exists.', 404);
    }
  }

  @Delete(':id')
  async deleteExam(@Param('id') id: number) {
    return await this.examService.deleteExam(id);
  }
}
