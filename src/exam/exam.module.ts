import { Field } from './../field/entities/field.entity';
import { Exam } from './entities/exam.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExamController } from './exam.controller';
import { ExamService } from './exam.service';
import { Module } from '@nestjs/common';

@Module({
  controllers: [ExamController],
  providers: [ExamService],
  imports: [TypeOrmModule.forFeature([Exam])],
})
export class ExamModule {}
