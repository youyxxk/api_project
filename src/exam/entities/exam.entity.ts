/* eslint-disable prettier/prettier */
import { ManyToOne } from 'typeorm';
import { Level } from './../../level/entities/level.entity';
import { Field } from './../../field/entities/field.entity';
import { Details } from 'src/details/entities/details.entity';
import { History } from './../../history/entities/history.entity';
import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('exams')
export class Exam {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'int', nullable: true})
  questionNumber: number;

  @Column({type: 'int', nullable: true})
  time: number;

  @OneToMany(
    () => History,
    (history: History) => history.exam,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  histories: History[];

  @OneToMany(
    () => Details,
    (details: Details) => details.exam,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  details: Details[];

  @ManyToOne(
    () => Field,
    (field: Field) => field.exams,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'}
  )
  @JoinColumn({name: 'fieldId'})
  field: Field;

  @ManyToOne(
    () => Level,
    (level: Level) => level.exams,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'}
  )
  @JoinColumn({name: 'levelId'})
  level: Level;
}
