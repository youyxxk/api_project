/* eslint-disable prettier/prettier */
import { Question } from './../question/entities/question.entity';
import { Field } from './../field/entities/field.entity';
import { Details } from 'src/details/entities/details.entity';
import { getRepository } from 'typeorm';
import { FieldService } from './../field/field.service';
import { Exam } from './entities/exam.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class ExamService {
  private fieldService = new FieldService(new Repository<Field>());
  constructor(
    @InjectRepository(Exam)
    private readonly examRepository: Repository<Exam>,
  ) {}

  async getAllExams(): Promise<Exam[]> {
    return await this.examRepository
      .createQueryBuilder('ex')
      .leftJoinAndSelect('ex.field', 'field')
      .leftJoinAndSelect('ex.level', 'level')
      .getMany();
  }

  async getExam(id: number): Promise<Exam> {
    return await this.examRepository
      .createQueryBuilder('ex')
      .leftJoinAndSelect('ex.field', 'field')
      .leftJoinAndSelect('ex.level', 'level')
      .where('ex.id = :exId', { exId: id })
      .getOne();
  }

  async getQuestionsByExamId(id: number) {
    return await getRepository(Question)
      .createQueryBuilder('qs')
      .where((qb) => {
        const subQuery = qb
          .subQuery()
          .select('dt.question.id')
          .from(Details, 'dt')
          .where('dt.exam.id = :id')
          .getQuery();
        return 'qs.id IN ' + subQuery;
      })
      .setParameter('id', id)
      .getMany();
  }

  async createExam(exam: Exam) {
    await this.examRepository.save(exam);
    return exam;
  }

  async updateExam(exam: Exam) {
    return await this.examRepository.save(exam);
  }

  async deleteExam(id: number) {
    return await this.examRepository.delete(id);
  }
}
