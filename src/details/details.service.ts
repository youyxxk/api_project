/* eslint-disable prettier/prettier */
import { User } from './../user/entities/user.entity';
import { Exam } from './../exam/entities/exam.entity';
import { getRepository } from 'typeorm';
import { Question } from './../question/entities/question.entity';
import { Details } from './entities/details.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class DetailsService {
  constructor(
    @InjectRepository(Details)
    private readonly detailsRepository: Repository<Details>,
  ) {}

  async getAllDetails(): Promise<Details[]> {
    return await this.detailsRepository
      .createQueryBuilder('dt')
      .innerJoin('dt.user', 'user')
      .innerJoin('dt.exam', 'exam')
      .innerJoin('dt.question', 'question')
      .select([
        'dt.selectedAnswer as selectedAnswer',
        'user.id as user',
        'exam.id as exam',
        'question.id as question',
      ])
      .getRawMany();
  }

  async getDetailsByUserId(userId: number): Promise<Details[]> {
    return await this.detailsRepository
      .createQueryBuilder('dt')
      .innerJoin('dt.user', 'user')
      .innerJoin('dt.exam', 'exam')
      .innerJoin('dt.question', 'question')
      .select([
        'dt.selectedAnswer as selectedAnswer',
        'user.id as user',
        'exam.id as exam',
        'question.id as question',
      ])
      .where('user.id = :id', { id: userId })
      .getRawMany();
  }

  async getDetailsExam(examId: number, userId: number) {
    return await this.detailsRepository
      .createQueryBuilder('dt')
      .leftJoin('dt.user', 'user')
      .leftJoin('dt.exam', 'exam')
      .leftJoinAndSelect('dt.question', 'question')
      .where('exam.id = :examId', { examId: examId })
      .andWhere('user.id = :userId', { userId: userId })
      .addSelect('user.id')
      .addSelect('exam.id')
      .getMany();
  }

  async getDetailsByExamId(examId: number): Promise<Details[]> {
    return await this.detailsRepository
      .createQueryBuilder('dt')
      .innerJoin('dt.user', 'user')
      .innerJoin('dt.exam', 'exam')
      .innerJoin('dt.question', 'question')
      .select([
        'dt.selectedAnswer as selectedAnswer',
        'user.id as user',
        'exam.id as exam',
        'question.id as question',
      ])
      .where('exam.id = :id', { id: examId })
      .getRawMany();
  }

  async getQuestionByExamId(id: number) {
    return await this.detailsRepository
      .createQueryBuilder('dt')
      .select(['dt.exam as exam', 'dt.question as question'])
      .where('dt.exam.id = :id', { id: id })
      .getRawMany();
  }

  async getUsersByExamId(id: number) {
    return await getRepository(User)
      .createQueryBuilder('user')
      .where((qb) => {
        const subQuery = qb
          .subQuery()
          .select('dt.user.id')
          .distinct(true)
          .from(Details, 'dt')
          .where('dt.exam.id = :id')
          .getQuery();
        return 'user.id IN ' + subQuery;
      })
      .setParameter('id', id)
      .getMany();
  }

  async createDetails(details: Details) {
    return this.detailsRepository.save(details);
  }

  async updateDetails(
    userId: number,
    examId: number,
    questionId,
    selectedAnswer: string,
  ) {
    const detail = await this.detailsRepository
      .createQueryBuilder('dt')
      .innerJoinAndSelect('dt.user', 'user')
      .innerJoinAndSelect('dt.exam', 'exam')
      .innerJoinAndSelect('dt.question', 'question')
      .where('user.id = :userId', { userId: userId })
      .andWhere('exam.id = :examId', { examId: examId })
      .andWhere('question.id = :questionId', { questionId: questionId })
      .getOne();
    detail.selectedAnswer = selectedAnswer;
    return this.detailsRepository.save(detail);
  }

  async updateExam(
    id: number,
    insertQuestions: Question[],
    deleteQuestions: Question[],
  ) {
    const check = insertQuestions.length < deleteQuestions.length;
    const loop = (check) ? insertQuestions.length : deleteQuestions.length;

    let i = 0;
    for (i; i < loop; i++) {
      await this.updateQuestionInExam(
        id,
        insertQuestions[i],
        deleteQuestions[i],
      );
    }

    if (check) {
      for (i; i < deleteQuestions.length; i++) {
        this.deleteQuestionInExam(id, deleteQuestions[i]);
      }
    } else {
      const users = await this.getUsersByExamId(id);
      const exam = await getRepository(Exam).findOne(id);

      for (i; i < insertQuestions.length; i++) {
        for (let j = 0; j < users.length; j++) {
          const details = new Details();
          details.exam = exam;
          details.question = insertQuestions[i];
          details.user = users[j];
          this.createDetails(details);
        }
      }
    }
  }

  async updateQuestionInExam(
    id: number,
    insertQuestion: Question,
    deleteQuestion: Question,
  ) {
    await this.detailsRepository
      .createQueryBuilder('dt')
      .update(Details)
      .set({
        question: insertQuestion,
      })
      .where('exam.id = :id', { id: id })
      .andWhere('question = :question', { question: deleteQuestion })
      .execute();
  }

  async deleteQuestionInExam(examId: number, question: Question) {
    return await this.detailsRepository
      .createQueryBuilder('dt')
      .delete()
      .where('exam.id = :id', { id: examId })
      .andWhere('question = :question', { question: question })
      .execute();
  }
}
