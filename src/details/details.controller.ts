/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Get,
  HttpException,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { DetailsService } from './details.service';
import { Details } from './entities/details.entity';
import { Question } from './../question/entities/question.entity';
import { Exam } from './../exam/entities/exam.entity';
import { User } from './../user/entities/user.entity';
@Controller('details')
export class DetailsController {
  constructor(private readonly detailsService: DetailsService) {}

  @Get()
  async getDetailExam(
    @Query('exam') examId: number,
    @Query('user') userId: number,
  ) {
    try {
      if (!examId || !userId) return await this.detailsService.getAllDetails();
      return await this.detailsService.getDetailsExam(examId, userId);
    } catch (error) {
      throw new HttpException('Details does not exists.', 404);
    }
  }

  @Get('user/:id')
  async getDetailsByUserId(@Param('id') id: number) {
    try {
      return await this.detailsService.getDetailsByUserId(id);
    } catch (error) {
      throw new HttpException('Details does not exists.', 404);
    }
  }

  @Get('exam/:id')
  async getDetailsByExamId(@Param('id') id: number) {
    try {
      return await this.detailsService.getDetailsByExamId(id);
    } catch (error) {
      throw new HttpException('Details does not exists.', 404);
    }
  }

  @Get('question')
  async getQuestionByExamId(@Query('exam') id: number) {
    return await this.detailsService.getQuestionByExamId(id);
  }

  @Post()
  async createDetails(
    @Body('selectedAnswer') selectedAnswer: string,
    @Body('user') user: User,
    @Body('exam') exam: Exam,
    @Body('question') question: Question,
  ) {
    try {
      const details = new Details();
      details.selectedAnswer = selectedAnswer;
      details.user = user;
      details.exam = exam;
      details.question = question;
      const data = await this.detailsService.createDetails(details);
      return {
        message: 'Details created.',
        data,
      };
    } catch (error) {
      throw new HttpException('user, exam or question is not valid.', 500);
    }
  }

  @Post('many')
  async createManyDetails(@Body() details: Details[]) {
    const indexErr = [];
    for (let i = 0; i < details.length; i++) {
      try {
        await this.detailsService.createDetails(details[i]);
      } catch (err) {
        indexErr.push(i);
      }
    }

    return {
      message: indexErr.length
        ? `Something wrong at index ${indexErr.toString()}`
        : `All details has been created.`,
    };
  }

  @Put()
  async updateDetails(
    @Query('user') userId: number,
    @Query('exam') examId: number,
    @Query('question') questionId: number,
    @Body('selectedAnswer') selectedAnswer: string,
  ) {
    try {
      await this.detailsService.updateDetails(
        userId,
        examId,
        questionId,
        selectedAnswer,
      );
      return { message: 'Details updated.' };
    } catch (error) {
      throw new HttpException('Details does not exists.', 404);
    }
  }

  @Put('exam/:id')
  async updateExam(
    @Param('id') id: number,
    @Body('insertQuestions') inserts: Question[],
    @Body('deleteQuestions') deletes: Question[],
  ) {
    return await this.detailsService.updateExam(id, inserts, deletes);
  }
}
