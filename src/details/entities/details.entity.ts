/* eslint-disable prettier/prettier */
import { Column, Entity } from 'typeorm';
import { Exam } from 'src/exam/entities/exam.entity';
import { Question } from './../../question/entities/question.entity';
import { User } from './../../user/entities/user.entity';
import { JoinColumn, ManyToOne } from 'typeorm';

@Entity('details')
export class Details {
  @Column({type: 'varchar', nullable: true})
  selectedAnswer: string;

  @ManyToOne(
    () => User,
    (user: User) => user.details,
    {onDelete: 'CASCADE', onUpdate: 'CASCADE', primary: true},
  )
  @JoinColumn({name: 'userId'})
  user: User;

  @ManyToOne(
    () => Exam,
    (exam: Exam) => exam.details,
    {onDelete: 'CASCADE', onUpdate: 'CASCADE', primary: true},
  )
  @JoinColumn({name: 'examId'})
  exam: Exam;

  @ManyToOne(
    () => Question,
    (question: Question) => question.details,
    {onDelete: 'CASCADE', onUpdate: 'CASCADE', primary: true},
  )
  @JoinColumn({name: 'questionId'})
  question: Question;
}
