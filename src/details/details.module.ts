import { Details } from 'src/details/entities/details.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { DetailsController } from './details.controller';
import { DetailsService } from './details.service';

@Module({
  controllers: [DetailsController],
  providers: [DetailsService],
  imports: [TypeOrmModule.forFeature([Details])],
})
export class DetailsModule {}
