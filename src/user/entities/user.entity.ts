/* eslint-disable prettier/prettier */
import { History } from './../../history/entities/history.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Details } from 'src/details/entities/details.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: "varchar", nullable: false, unique: true})
  name: string;

  @Column({type: "varchar", nullable: false})
  password: string;

  @Column({type: "varchar", nullable: true})
  phone: string;

  @Column({type: "varchar", nullable: true})
  address: string;

  @Column({type: "varchar", nullable: true})
  email: string;

  @Column({type: 'int', nullable: true})
  authentication: number;

  @Column({type: "bool", default: false})
  role: boolean;

  @OneToMany(
    () => History,
    (history: History) => history.user,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  histories: History[];

  @OneToMany(
    () => Details,
    (details: Details) => details.user,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  details: Details[];
}
