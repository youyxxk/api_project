/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-empty-function */
import { UserService } from './user.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { User } from './entities/user.entity';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async getAllUsers() {
    return await this.userService.getAllUsers();
  }

  @Get(':id')
  async getUserById(@Param('id') userId: number) {
    return await this.userService.getUserById(userId);
  }

    @Get('name/:name')
    async getUserByName(@Param('name') name: string) {
      return await this.userService.getUserByName(name);
    }

  @Post()
  async createUser(@Body() user: User) {
    const data = await this.userService.createUser(user);
    return {
      message: 'User created.',
      data,
    };
  }

  @Put(':id')
  async updateUser(
    @Param('id') userId: number,
    @Body()
    { password, phone, address, email, authentication }: User,
  ) {
    const user = await this.userService.getUserById(userId);
    
    if (password) {
      user.password = password;
    }
    user.phone = phone;
    user.address = address;
    user.email = email;
    user.authentication = authentication;
    this.userService.updateUser(user);
    return { message: 'User updated.' };
  }

  @Delete(':id')
  deleteUser(@Param('id') userId: number) {
    return this.userService.deleteUser(userId);
  }
}
