/* eslint-disable prettier/prettier */
export class CreateUserDTO {
  public name: string;
  public password: string;
  public phone: string;
  public address: string;
  public email: string;
  public role: boolean;
}
