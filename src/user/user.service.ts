import { NotFoundException } from '@nestjs/common';
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-empty-function */
import { User } from './entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async getAllUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async getUserById(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);
    if(!user) throw new NotFoundException('User does not exists.');
    return user;
  }

  async getUserByName(name: string): Promise<User> {
    const user = await this.userRepository.findOne({name: name});
    if(!user) throw new NotFoundException('User does not exists.');
    return user;
  }

  async createUser(user: User) {
    return await this.userRepository.save(user);
  }

  async updateUser(user: User) {
    return await this.userRepository.save(user);
  }

  async deleteUser(id: number) {
    return await this.userRepository.delete(id);
  }
}
