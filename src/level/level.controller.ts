import { Level } from './entities/level.entity';
import { LevelService } from './level.service';
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  NotFoundException,
} from '@nestjs/common';

@Controller('level')
export class LevelController {
  constructor(private readonly levelService: LevelService) {}

  @Get()
  async getAllLevels() {
    return await this.levelService.getAllLevels();
  }

  @Get(':id')
  async getLevel(@Param('id') id: number) {
    return await this.levelService.getLevel(id);
  }

  @Post()
  async createLevel(@Body() level: Level) {
    await this.levelService.createLevel(level);
    return {
      message: 'Level created.',
      level,
    };
  }

  @Put(':id')
  async updateLevel(
    @Param('id') id: number,
    @Body()
    { label }: Level,
  ) {
    const level = await this.levelService.getLevel(id);
    if (!level) throw new NotFoundException('level does not exists');
    if (label) {
      level.label = label;
    }

    this.levelService.updateLevel(level);
    return { message: 'level updated.' };
  }

  @Delete(':id')
  deleteLevel(@Param('id') userId: number) {
    return this.levelService.deleteLevel(userId);
  }
}
