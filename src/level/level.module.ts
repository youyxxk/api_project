import { Level } from './entities/level.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { LevelController } from './level.controller';
import { LevelService } from './level.service';

@Module({
  controllers: [LevelController],
  providers: [LevelService],
  imports: [TypeOrmModule.forFeature([Level])],
})
export class LevelModule {}
