/* eslint-disable prettier/prettier */
import { Exam } from 'src/exam/entities/exam.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Question } from './../../question/entities/question.entity';

@Entity('levels')
export class Level {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: true })
  label: string;

  @OneToMany(
    () => Question,
    (question: Question) => question.level,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  questions: Question[];

  @OneToMany(
    () => Exam,
    (exam: Exam) => exam.level,
    {onUpdate: 'CASCADE', onDelete: 'CASCADE'},
  )
  exams: Exam[];
}
