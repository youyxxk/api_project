/* eslint-disable prettier/prettier */
import { Level } from './entities/level.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NotFoundException } from '@nestjs/common';

@Injectable()
export class LevelService {
  constructor(
    @InjectRepository(Level)
    private readonly levelRepository: Repository<Level>
  ) {}

  async getAllLevels(): Promise<Level[]> {
    return await this.levelRepository.find();
  }

  async getLevel(id: number): Promise<Level> {
    try {
      const level = await this.levelRepository.findOne(id);
      return level;
    } catch (error) {
      throw new NotFoundException('level does not exists.');
    }
  }

  async createLevel(level: Level) {
    return await this.levelRepository.save(level);
  }

  async updateLevel(level: Level) {
    return await this.levelRepository.save(level);
  }

  async deleteLevel(id: number) {
    return await this.levelRepository.delete(id);
  }
}
